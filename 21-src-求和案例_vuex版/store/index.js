import Vue from "vue";
//引入vuex
import Vuex from "vuex"
//应用插件Vuex
Vue.use(Vuex)
//准备actions  用于响应组件中的动作
const actions = {
    /*increment: (context, value) => {
        // console.log(context, value)
        context.commit('INCREMENT', value)
    },
    decrement: (context, value) => {
        // console.log(context, value)
        context.commit('DECREMENT', value)
    },*/
    incrementOdd: (context, value) => {
        // console.log(context, value)
        if (context.state.sum % 2) {
            context.commit('INCREMENT', value)
        }
    },
    incrementDelay: (context, value) => {
        // console.log(context, value)
        setTimeout(() => {
            context.commit('INCREMENT', value)
        },500)
    },
}
//准备mutations  用于操作数据（state）
const mutations = {
    INCREMENT: (state, value) => {
        state.sum += value
    },
    DECREMENT: (state, value) => {
        state.sum -= value
    }
}
//准备state  用于存储数据
const state = {
    sum: 0,
}
export default new Vuex.Store({
    actions,
    mutations,
    state
})

