import Vue from "vue";
import App from "@/App";

Vue.config.productionTip = false

new Vue({
    el: '#app',
    render: h => h(App),
    // 安装全局时间总线
    beforeCreate() {
        Vue.prototype.$bus = this
    }
})