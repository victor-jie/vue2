// 引入路由
import VueRouter from "vue-router";

import About from "@/pages/About";
import Home from "@/pages/Home";
import Message from "@/pages/Message";
import News from "@/pages/News";
import Details from "@/pages/Details";

const router = new VueRouter({
    mode:'history',//默认为hash
    routes: [
        {
            name: 'home',
            path: '/home',
            component: Home,
            meta: {isAuth: true,title: '主页'},
            children: [
                {
                    name: 'message',
                    path: 'message',
                    component: Message,
                    meta: {isAuth: true, title: '信息'},
                    children: [
                        {
                            name: 'detail',
                            path: 'details/:id/:title',
                            component: Details,
                            meta: {title: '主页'},
                            //第一种写法: props值为对象，该对象中所有的key-value的组合最终都会通过props传给Detail组件
                            // props:{a:'321',b:'b'},

                            //第二种写法: props值为布尔值，值为true，则把路由收到的所有params参数通过props传给Detail组件
                            // props:true,

                            //第三种写法: props值为函数，该函数返回的对象中每一组key-value都会通过props传给Detail组件
                            /*props($route){
                                return{
                                    id:$route.params.id,
                                    title:$route.params.title
                                }
                            }*/
                            props($router) {
                                return {
                                    id: $router.params.id,
                                    title: $router.params.title
                                }
                            }
                        }
                    ]
                },
                {
                    name: 'news',
                    path: 'news',
                    component: News,
                    meta: {isAuth: true, title: '新闻'},
                    /*beforeEnter(to,from,next){
                        if (to.meta.isAuth) { // 判断是否鉴权
                            if (localStorage.getItem('name') === 'victo') {
                                next()
                            } else {
                                alert('name错误,无法访问')
                            }
                        } else {
                            next()
                        }
                    }*/
                }
            ]
        },
        {
            name: 'about',
            path: '/about',
            component: About,
            meta: {title: '关于'}
        },
    ]
})

//全局前置路由守卫——初始化的时候被调用、每次路由切换之前被调用
/*router.beforeEach((to, from, next) => {
    // if(to.name === 'news' || to.name === 'message'){
    if (to.meta.isAuth) { // 判断是否鉴权
        if (localStorage.getItem('name') === 'victo') {
            next()
        } else {
            alert('name错误,无法访问')
        }
    } else {
        next()
    }
})*/

//全局后置路由守卫——初始化的时候被调用、每次路由切换之后被调用
router.afterEach((to, from) => {
    document.title = to.meta.title || 'vuejs'
})

export default router