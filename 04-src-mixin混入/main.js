import Vue from "vue";
import App from "@/App";

import {all} from "@/mixin";

Vue.config.productionTip = false

Vue.mixin(all)//全局混入

new Vue({
    el: '#app',
    render: h => h(App)
})