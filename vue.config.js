const {defineConfig} = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    // 关闭错误提示
    lintOnSave: false,
    //开启代理服务器
    // devServer: {
    //     proxy: 'http://localhost:8000'
    // }
})
