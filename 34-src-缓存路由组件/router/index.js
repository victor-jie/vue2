// 引入路由
import VueRouter from "vue-router";

import About from "@/pages/About";
import Home from "@/pages/Home";
import Message from "@/pages/Message";
import News from "@/pages/News";
import Details from "@/pages/Details";

export default new VueRouter({
    routes: [
        {
            path: '/home',
            component: Home,
            children: [
                {
                    path: 'message',
                    component: Message,
                    children:[
                        {
                            name:'detail',
                            path:'details/:id/:title',
                            component:Details,
                            //第一种写法: props值为对象，该对象中所有的key-value的组合最终都会通过props传给Detail组件
                            // props:{a:'321',b:'b'},

                            //第二种写法: props值为布尔值，值为true，则把路由收到的所有params参数通过props传给Detail组件
                            // props:true,

                            //第三种写法: props值为函数，该函数返回的对象中每一组key-value都会通过props传给Detail组件
                            /*props($route){
                                return{
                                    id:$route.params.id,
                                    title:$route.params.title
                                }
                            }*/
                            props($router){
                                return {
                                    id:$router.params.id,
                                    title:$router.params.title
                                }
                            }
                        }
                    ]
                },
                {
                    path: 'news',
                    component: News
                }
            ]
        },
        {
            name:'about',
            path: '/about',
            component: About
        },
    ]
})