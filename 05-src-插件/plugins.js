export default {
    install(Vue) {
        // 全局过滤
        Vue.filter('setSlice', function (value) {
            return value.slice(0, 4)
        })
        // 自定义指令
        Vue.directive('fbind', {
            // 指令与元素成功绑定时(一上来）
            bind(element, binding) {
                element.value = binding.value
            },
            // 指令所在元素被插入页面时
            inserted(element, binding) {
                element.focus()
            },
            // 指令所在的模板被重新解析时
            update(element, binding) {
                element.value = binding.value
            }
        })
        // 全局混入
        Vue.mixin({
            data() {
                return {
                    x: 123,
                    y: 333
                }
            }
        })
        // 在Vue原型上添加方法
        Vue.prototype.aini = () => {
            alert('小可爱！！！')
        }
    }
}