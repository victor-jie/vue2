import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
    el: '#root',
    render: h => h(App),// 等于components{App}注册组件
})
