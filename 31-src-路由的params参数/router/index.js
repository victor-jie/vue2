// 引入路由
import VueRouter from "vue-router";

import About from "@/pages/About";
import Home from "@/pages/Home";
import Message from "@/pages/Message";
import News from "@/pages/News";
import Details from "@/pages/Details";

export default new VueRouter({
    routes: [
        {
            path: '/home',
            component: Home,
            children: [
                {
                    path: 'message',
                    component: Message,
                    children:[
                        {
                            name:'detail',
                            path:'details/:id/:title',
                            component:Details
                        }
                    ]
                },
                {
                    path: 'news',
                    component: News
                }
            ]
        },
        {
            name:'about',
            path: '/about',
            component: About
        },
    ]
})