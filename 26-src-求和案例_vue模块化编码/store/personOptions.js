
// 人员相关配置
import axios from "axios";
import {nanoid} from "nanoid";

export default {
    namespaced: true,
    actions: {
        personWang(context, value) {
            if (value.name.indexOf('王') === 0) {
                context.commit('ADD_PERSON', value)
            } else {
                alert('这人不姓王！')
            }
        },
        addPersonServe(context){
            axios.get('https://api.uixsj.cn/hitokoto/get?type=social').then(
                (response) =>{
                    context.commit('ADD_PERSON',{id:nanoid(),name:response.data})
                },
                (error) =>{
                    alert(error.message)
                }
            )
        }
    },
    mutations: {
        ADD_PERSON: (state, value) => {
            state.personList.unshift(value)
        }
    },
    state: {
        personList: [
            {id: nanoid(), name: '张三'}
        ]
    },
    getters: {
        firstPersonName(state) {
            return state.personList[0].name
        }
    }
}