
// 求和相关配置
export default {
    namespaced: true,
    actions: {
        incrementOdd: (context, value) => {
            if (context.state.sum % 2) {
                context.commit('INCREMENT', value)
            }
        },
        incrementDelay: (context, value) => {
            setTimeout(() => {
                context.commit('INCREMENT', value)
            }, 500)
        },
    },
    mutations: {
        INCREMENT: (state, value) => {
            state.sum += value
        },
        DECREMENT: (state, value) => {
            state.sum -= value
        },
    },
    state: {
        sum: 0,
        name: '刘杰',
        age: 18,
    },
    getters: {
        bigSum: (state) => {
            return state.sum * 10
        },
    }
}