import Vue from "vue";
//引入vuex
import Vuex from "vuex"
import countOptions from '@/store/countOptions'
import personOptions from "@/store/personOptions";

//应用插件Vuex
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        countOptions,
        personOptions
    }
})

