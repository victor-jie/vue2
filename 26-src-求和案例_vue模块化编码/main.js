import Vue from "vue";
import App from "@/App";
//引入store
import store from "@/store";

Vue.config.productionTip = false

new Vue({
    el: '#app',
    render: h => h(App),
    // 配置store
    store,
    // 安装全局时间总线
    beforeCreate() {
        Vue.prototype.$bus = this
    }
})