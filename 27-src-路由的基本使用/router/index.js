// 引入路由
import VueRouter from "vue-router";

import About from "@/pages/About";
import Home from "@/pages/Home";

export default new VueRouter({
    routes: [
        {
            path: '/home',
            component: Home
        },
        {
            path: '/about',
            component: About
        },
    ]
})